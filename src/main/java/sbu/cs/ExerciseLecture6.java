package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr)
    {
        long Sum = 0;
        for(int len = 0; len < arr.length; len += 2)
        {
            Sum += arr[len];
        }
        return Sum;

    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr)
    {
        int [] Reversed = new int [arr.length];

        for(int i = arr.length - 1, j = 0; i >= 0; i--, j++)
        {
            Reversed[j] = arr[i];
        }
        return Reversed;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {
        if(m2.length != m1[0].length)
        {
            throw new IllegalValueException();
        }

        double[][] Product = new double[m1.length] [m2[0].length];
        for(int i = 0; i < m1.length; i++)
        {
            for (int j = 0; j < m2[0].length; j++)
            {
                for (int k = 0; k < m1[0].length; k++)
                {
                    Product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return Product;
    }
    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names)
    {
        List<List<String>> NameList = new ArrayList<List<String>>();
        for(int i = 0; i < names.length; i++)
        {
            ArrayList<String> temp = new ArrayList<>();
            for(int j = 0; j < names[0].length; j++)
            {
                temp.add(names[i][j]);
            }
            NameList.add(temp);
        }
        return NameList;

    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n)
    {
        List<Integer> Prime = new ArrayList<Integer>();
        for(int i = 2 ; i <= n ; i++)
        {
            if(n % i == 0)
            {
                Prime.add(i) ;
            }
        }
        for(int i = 0; i < Prime.size() ; i++)
        {
            int Entrance = Prime.get(i) ;
            for(int j = 2; j< Entrance; j++)
            {
                if(Entrance %j==0)
                {
                    Prime.remove(i) ;
                    i = i - 2 ;
                    break;
                }
            }
        }
        return Prime;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {
        line = line.replaceAll("[!@#$%^&*()?\\-_~,/]*", "");
        String [] words = line.split(" ");
        List<String> Ans = new ArrayList<String>();
        for(int i = 0; i < words.length; i ++)
        {
            Ans.add(words[i]);
        }
        return Ans;
    }
}

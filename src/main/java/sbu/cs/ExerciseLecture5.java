package sbu.cs;
import java.util.*;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length)
    {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++)
        {
            sb.append((char)('a' + rnd.nextInt(26)));
        }
        return sb.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if (length < 3) {
            throw new IllegalValueException();
        }
        String special = "~@#$!*&](!";

        Random rnd = new Random();
        int Random = rnd.nextInt(10);
        int Chance = rnd.nextInt(100);
        StringBuilder password = new StringBuilder();
        password.append(Random);
        password.append(special.charAt(Random));
        for (int i = 0; i < length - 2; i++) {
            password.append((char) (rnd.nextInt(26) + 'a'));
        }
        if (Chance > 50)
        {
            password = password.reverse();
        }
        return password.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n)
    {
        ExerciseLecture4 EL4O = new ExerciseLecture4();
        int i = 1;
        long sum = 0, bin = 0;
        while(sum <= n) {
            long fiboI = EL4O.fibonacci(i);
            String IFiboBinary = Long.toBinaryString(fiboI);
            for(int j = 0; j < IFiboBinary.length(); j++) {
                if(IFiboBinary.charAt(j) == '1')
                {
                    bin++;
                }
            }
            sum = fiboI + bin;
            if(sum == n)
            {
                return true;
            }
            i++;
        }
        return false;
    }

}

package sbu.cs;
import java.util.*;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n)
    {
        int fact = 1;
        for(int i = 1; i <= n; i++ )
        {
            fact *=i;
        }
        return fact;
    }


    public long fibonacci(int n)
    {
        if(n == 1 || n == 2)
        {
            return 1;
        }


        return fibonacci(n-1) + fibonacci(n-2);
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word)
    {
        StringBuilder ans = new StringBuilder(word);

        return ans.reverse().toString() ;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line)
    {
        line = line.toUpperCase().replaceAll(" ","") ;
        StringBuilder sb = new StringBuilder(line) ;
        return line.equals(sb.reverse().toString()) ;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2)
    {
        char [][] DotPlot = new char[str1.length()][str2.length()];
        for(int row = 0; row < str1.length(); row ++)
        {
            for(int column = 0; column < str2.length(); column++)
            {
                if(str1.charAt(row) == str2.charAt(column))
                {
                    DotPlot[row][column] = '*';
                }
                else {
                    DotPlot[row][column] = ' ' ;
                }
            }
        }
        return DotPlot ;
    }
}

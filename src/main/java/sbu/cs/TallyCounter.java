package sbu.cs;

public class TallyCounter implements TallyCounterInterface
{
    private int Value = 0;
    @Override
    public void count()
    {
        if(Value != 9999)
        {
            Value++;
        }
    }
    @Override
    public int getValue()
    {
        return Value;
    }
    @Override
    public void setValue(int value) throws IllegalValueException
    {
        if(value < 0 || value > 9999)
        {
            throw new IllegalValueException();
        }

        else{
            this.Value = value;
        }
    }
}
